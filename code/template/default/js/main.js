$(document).ready(function() {
	$(".header .an").click(function() {
		$(".header .an").toggleClass("n");
		$("html").toggleClass("on");
		$(".nav").toggleClass("on");
		$(".e_j").slideUp();
		$(".s_j").slideUp();
	});
	$(".nav .y_j i").click(function() {
		$(this).parent().siblings(".e_j").slideToggle();
		$(this).parents(".nav ul li").siblings().find(".e_j").slideUp();
		$(this).parents(".nav ul li").find(".s_j").slideUp();
	});

	$(".w_x").click(function() {
		$(".e_m").slideDown();
	});
	$(".e_m").click(function() {
		$(this).slideUp();
	});
	$(".f_h").click(function() {
		$("html,body").animate({
			scrollTop: 0
		}, 500);
	});
});
$(function() {
	$(window).scroll(function() {
		var _top = $(window).scrollTop();
		if (_top > 40) {
			$(".header").addClass("on");
			$(".f_h").fadeIn(300);
		} else {
			$(".header").removeClass("on");
			$(".f_h").fadeOut(300);
		}
	});

	$(".logo2 .sc").click(function() {
		$(".logo2 .search").slideToggle();
	});
});
$(function() {
	var swiper = new Swiper('.sy_ban', {
		autoplay: {
			disableOnInteraction: false,
		},
		loop: true,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});
});


$(function() {
	$(".xsm ul dl").click(function() {
		$(this).toggleClass("show");
		$(this).siblings().removeClass("show");
	});
});
$(function() {
	$(".join ul dl").click(function() {
		$(this).toggleClass("show");
		$(this).siblings().removeClass("show");
	});
});

$(function() {
	$(".nyj2").click(function() {
		$(".nej").toggle(300);
	});
});
