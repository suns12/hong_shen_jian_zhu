<!doctype html>

<html>
    <head>
    <meta charset="utf-8">
    <title><?php echo $site_title; ?></title>
    <meta name="keywords" content="<?php echo $site_keywords; ?>" />
    <meta name="description" content="<?php echo $site_description; ?>" />
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, viewport-fit=cover">
    <link href="<?php echo $site_template; ?>css/swiper.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_template; ?>css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_template; ?>css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_template; ?>css/style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $site_template; ?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript"  src="<?php echo $site_template; ?>js/swiper.min.js"></script>
    <script type="text/javascript"  src="<?php echo $site_template; ?>js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript"  src="<?php echo $site_template; ?>js/wow.min.js"></script>
    <script type="text/javascript"  src="<?php echo $site_template; ?>js/main.js"></script>
    <script type="text/javascript"  src="<?php echo $site_template; ?>js/jquery.SuperSlide.2.1.1.js"></script>
    <script type="text/javascript"  src="<?php echo $site_template; ?>js/wow.min.js"></script>
    <link href="<?php echo $site_template; ?>static/css/animate.css,iconfont.css,bootstrap.min.css,response.min.css,respagestyle.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $site_template; ?>static/css/ftmpl_impt_370a115a-cb34-4cf5-a1f7-461ed28c5d4e.css" rel="stylesheet" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, viewport-fit=cover">
    <script type="text/javascript">
    window.siteIsPc=true;
    window.foreignList= [];
    window.tenant = {"accessPool":"pool6","appList":[],"attr":null,"bossProductInstance":"","cdnFlag":"0","ceYun":true,"createTime":{"date":12,"day":4,"hours":10,"minutes":34,"month":11,"seconds":59,"time":1576118099000,"timezoneOffset":-480,"year":119},"cusId":"","domain":"www.jsqj.cn","domainInfo":null,"domainStatus":2,"domains":[],"foreign":false,"groupId":"","hotelCusdomain":"","hotelDomain":"","id":170588,"instanceList":[{"appCodes":[],"bossProductInstance":"NEW2019121210443329067","cusId":"0qmqr3bupf2GVGcJE38wEm","flag":1,"id":238177,"insertDate":{"date":12,"day":4,"hours":10,"minutes":44,"month":11,"seconds":45,"time":1576118685000,"timezoneOffset":-480,"year":119},"memberId":"CEM10105060","parent":null,"parentId":0,"productDomain":"","productionId":"100001","producttype":1,"tenant":null,"tenantApp":null,"tenantCode":"","tenantId":0,"tenants":[],"trade":"DOMESTIC_TRADE","unittype":0}],"language":"zh_CN","makeDomain":"1912125029.pool6-site.make.yun300.cn","man":1,"mobileDomain":"","mobileMakeDomain":"1912125029.pool6-msite.make.yun300.cn","mobileProductDomain":"1912125029.pool6-msite.yun300.cn","mobilePublishTime":{"date":8,"day":3,"hours":14,"minutes":50,"month":6,"seconds":17,"time":1594191017000,"timezoneOffset":-480,"year":120},"mobileStatus":8,"mverify":"","mycat":"85","name":"","ningkerCusdomain":"","ningkerDomain":"DOMESTIC_TRADE","oganize":"[\"1\",\"2\"]","pcPublishTime":{"date":8,"day":3,"hours":14,"minutes":50,"month":6,"seconds":17,"time":1594191017000,"timezoneOffset":-480,"year":120},"pool":"pool6","productAttrId":0,"productDomain":"1912125029.pool6-site.yun300.cn","productInstance":{"appCodes":[],"bossProductInstance":"NEW2019121210443329067","cusId":"0qmqr3bupf2GVGcJE38wEm","flag":1,"id":238177,"insertDate":{"date":12,"day":4,"hours":10,"minutes":44,"month":11,"seconds":45,"time":1576118685000,"timezoneOffset":-480,"year":119},"memberId":"CEM10105060","parent":null,"parentId":0,"productDomain":"","productionId":"100001","producttype":1,"tenant":null,"tenantApp":null,"tenantCode":"","tenantId":0,"tenants":[],"trade":"DOMESTIC_TRADE","unittype":0},"productInstanceCode":"NEW2019121210443329067","productInstanceId":238177,"publishPool":"","rem":"888888","resourcesPath":"/888888/pool6","rusRoute":"pool6_888888","status":6,"targetPool":"","targetRem":"","templateCode":"core_whole_site","tenantCode":"100001_1912125029","tenantRelations":[],"tennatCode":"100001_1912125029","unittype":"100001","updateTime":{"date":28,"day":2,"hours":13,"minutes":23,"month":6,"seconds":37,"time":1595913817000,"timezoneOffset":-480,"year":120},"verify":"952fb7ea5af3778fa32364d7b5b3c352","youyiProductName":""};
    window.commonShortUrl = ("http://www.ceurl.cn" == "") ? "" : "http://www.ceurl.cn" + "/";
    window.upgradeVersion="9f3bdfacfd460f06c0f1f00c4377df93";
    var isxinnet = "false";
    window.noredirectCookieName = "_noredirect";
    var visittrack_siteId = "100001_1912125029";
    var visittrack_url = "";
    var gatherScripts = "";
    var unittype=window.tenant.unittype ;
    window.globalObj={};
    window.globalObj.isOpenSSL = false;
    try{
        var setDomain = window.location.hostname.replace("http://", "").replace("https://", "");
        if (setDomain.match(/[a-z]+/) != null) {
            var domainArr = setDomain.split(".");
            var preDomain=domainArr[domainArr.length - 2] + "."
                + domainArr[domainArr.length - 1];
            if(/(com|cn|org|net|xin|edu)\..*/.test(preDomain)){
                preDomain=domainArr[domainArr.length - 3]+"."+domainArr[domainArr.length - 2] + "."
                    + domainArr[domainArr.length - 1];
            }
            document.domain = preDomain;
        }
    }catch(e){
        console.log(e);
    }

</script>
    <script type="text/javascript" src="<?php echo $site_template; ?>static/js/8644b254c3614ee9a048ddf385f9520d.js"></script>
    <script>

        new WOW().init();

    </script>
    </head>

    <body>
<div class="logo">
      <div class="container">
    <div class="row">
          <div class="logo1 col-8 col-sm-8 col-md-4 col-lg-2 col-xl-2"> <a href="/"><img src="<?php $this->block(1);?>"></a> </div>
          <div class="col-4 col-sm-4 col-md-8 col-lg-8 col-xl-8">
        <div class="header">
              <div class="hda">
            <div class="row">
                  <div class="an"><span class="a1"></span><span class="a2"></span><span class="a3"></span></div>
                  <div class="nav">
                <ul>
                	<?php $aaa=$catid;$bbb=$parentid;$ccc=$cat[topid]; ?>
                      <li  class="<?php if ($index) { ?>on<?php } ?>">
                    <div class="y_j"><a href="/" class="o_lm">首页</a></div>
                  </li>
                   <li class="<?php if($aaa==1 || $bbb==1 || $ccc==1){?>on<?}?>">
                        <div class="y_j"><?php $return = $this->_category("catid=1");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>
                        <div class="e_j"> <?php $return = $this->_category("parentid=1");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a> <?php } ?> </div>
                      </li>
                      <li class="<?php if($aaa==2 || $bbb==2 || $ccc==2){?>on<?}?>">
                        <div class="y_j"><?php $return = $this->_category("catid=2");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>
                        <div class="e_j"> <?php $return = $this->_category("parentid=2");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a> <?php } ?> </div>
                      </li>
                      <li class="<?php if($aaa==3 || $bbb==3 || $ccc==3){?>on<?}?>">
                        <div class="y_j"><?php $return = $this->_category("catid=3");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>
                        <div class="e_j"> <?php $return = $this->_category("parentid=3");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a> <?php } ?> </div>
                      </li>
                      <li class="<?php if($aaa==4 || $bbb==4 || $ccc==4){?>on<?}?>">
                        <div class="y_j"><?php $return = $this->_category("catid=4");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>
                        <div class="e_j"> <?php $return = $this->_category("parentid=4");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a> <?php } ?> </div>
                      </li>
                      <li class="<?php if($aaa==5 || $bbb==5 || $ccc==5){?>on<?}?>">
                        <div class="y_j"><?php $return = $this->_category("catid=5");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>
                        <div class="e_j"> <?php $return = $this->_category("parentid=5");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a> <?php } ?> </div>
                      </li>
                      <li class="<?php if($aaa==6 || $bbb==6 || $ccc==6){?>on<?}?>">
                        <div class="y_j"><?php $return = $this->_category("catid=6");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>
                        <div class="e_j"> <?php $return = $this->_category("parentid=6");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a> <?php } ?> </div>
                      </li>
                      <li class="<?php if($aaa==7 || $bbb==7 || $ccc==7){?>on<?}?>">
                        <div class="y_j"><?php $return = $this->_category("catid=7");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>
                        <div class="e_j"> <?php $return = $this->_category("parentid=7");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a> <?php } ?> </div>
                      </li>
                    </ul>
              </div>
                  <div class="clear"></div>
                  <div class="xiaocms-page"><?php echo $pagelist; ?></div>
                </div>
          </div>
            </div>
      </div>
          <div class="logo2 col-sm-12 col-md-2 col-lg-2 col-xl-2">
        <div class="sc"><img src="<?php echo $site_template; ?>images/fd2.png"><a><?php $this->block(2);?></a></div>
      </div>
        </div>
  </div>
    </div>

<!--内大图--> 

<?php include $this->_include('left.html'); ?>
<div class="cplb">
      <div class="main pagebox">
    <div class="w_grid signal">
          <div class="e_box e_box-000 p_gridbox">
        <div id="content_box-1577952087203-0" class="e_box e_box-000 d_gridCell_0 p_gridCell">
              <div id="w_grid-1581304653154" class="w_grid-001" data-tmpleditable="editable">
            <div class="w_grid signal">
                  <div class="e_box e_box-000 p_gridbox">
                <div id="content_box-1581304653154-0" class="e_box e_box-000 d_gridCell_0 p_gridCell ND_empty" data-editablecontent="1581304653154">
                      <div id="w_grid-1581381956994" class="w_grid-001">
                    <div class="w_grid ">
                          <div class="e_box e_box-000 p_gridbox">
                        <div id="content_box-1581381956994-1" class="e_box e_box-000 d_gridCell_1 p_gridCell">
                              <div id="c_portalResBreadcrumb_nav-1581381957059" class="c_portalResBreadcrumb_nav-01001"> <!--homePage  首页-->
                            <div class="e_box e_box-000 p_breadcrumbList" data-ename="面包屑导航"> 
                                  
                                  <!--compositePage 应用综合页对象--> 
                                  <!--navBreadcrumb 应用数据集合对象（list集合）--> 
                                  
                                  <!-- currentPage 当前页对象--> 
                                  
                                </div>
                          </div>
                              <div id="c_portalResProduct_list-15813828277427729" class="c_portalResProduct_list-01001011"> 
                            <script type="text/javascript">$(function(){$('[id*="PhotoList_k"] ul').lightGallery({});});</script>
                            <link type="text/css" rel="stylesheet" href="<?php echo $site_template; ?>static/css/rbqbhvy9xyieltygaaaaaejcm9i116.css" />
                            <input type="hidden" class="listAddUrl" value="&gototype=add&productType=0&backType=picview&proshowcaseId=&proparentId=&appId=">
                            <input type="hidden" class="listModifyUrl" value="&proparentId=&appId=">
                            <div class="PhotoList_k1" id="PhotoList_k1">
                                  <ul class="e_box p_products " style="    overflow: hidden;">
                                  <?php $return = $this->_listdata("catid=$catid page=$page"); extract($return); if (is_array($return))  foreach ($return as $key=>$xiao) { ?>
                                  
                                <li data-src="<?php echo image($xiao[thumb]); ?>"> <a href="<?php echo $xiao['url']; ?>" title="<?php echo $xiao['title']; ?>"> <img class="img-responsive" src="<?php echo image($xiao[thumb]); ?>" alt="<?php echo $xiao['title']; ?>">
                                  <h4><?php echo $xiao['title']; ?></h4>
                                  </a> </li>
                                  
                                <?php } ?>
                                
                              </ul>
                              
				<div class="xiaocms-page"><?php echo $pagelist; ?></div>
                                </div>
                                
                            <script type="text/javascript">
  $comp({
      textParams: {
          isCutOfName: true, //标题是否自动截取超长显示内容
          linesOfCutOfName: 1, //标题超出隐藏的行数设置
          isCutOfSummary: true, //概述是否自动截取超长显示内容
          linesOfCutOfSummary: 1, //概述超出隐藏的行数设置
          categorySummary: true, //分类概述是否自动截取超长显示内容
          categorySummaryMaxLen: 2 //分类概述超出隐藏的行数设置
      },
      picParams: {
          isOpenCutMode: true, //是否开启裁剪 不开启false
          isRate: true, //是否按比例裁剪
          widthOfRate: 1, //宽度的比率
          heightOfRate: 1, //高度的比率
          definedDefaultImage: false, //是否设置默认图片
          defaultImage: "", //设置默认图片的值
          appId: 2
      },
      ctx: "",
      appId: "2",
      productMode: "",
      requestParam:{"orderType":null,"xibcommonId":"9","appType":"productCateId","orderColumn":null,"productCateId":"9"},
      pageParam: {
          url: "/comp/portalResProduct/list.do?compId=portalResProduct_list-15813828277427729",
          appId: "2",
          turnPageType:"turnPage"
      },
      id: "portalResProduct_list-15813828277427729",
      lib: ["js/portalProduct/portalResProduct_list-01"],
      imageOpts: {
          "comp_w_size": "", //容器宽
          /*大图*/
          //列表类传值为空，避免图片外层盒子创建2次
          "box_img_style_0": null,
          "box_img_style_1": null,
          "box_img_style_2": null,

          "e_img_style_0": "e_image",
          "e_img_style_1": "e_image-000",
          "e_img_style_2": "p_image",
          "e_a_img_style_0": "e_link",
          "e_a_img_style_1": "e_link-000",
          "e_a_img_style_2": "p_linkB",
          "e_a_txt_style_0": "e_link",
          "e_a_txt_style_1": "e_link-000",
          "e_a_txt_style_2": "p_linkA",

          //大图宽高比例
          "img_scale_x": "4", //宽
          "img_scale_y": "3", //高
          "img_transition": "fade", //图片动画效果
          "img_speed": 1000, //效果显示速度
          "img_easing": "easeOutQuart", //动画效果

          //大图高清裁剪, 普通：normal; 高清： hd; 裁剪: cut; 高清和裁剪： both;
          "img_display_type": "cut",
          "box_nav_style_1": "e_box-000",
          "box_nav_style_0": "e_box",
          "box_nav_style_2": "p_ThumbnailBox",
          "e_nav_img_style_1": "e_ProductRelationImg-001",
          "e_nav_img_style_0": "e_relationimg",
          "e_nav_img_style_2": "p_thumbnail",
          "nav_display_status": true, //规格图是否显示，true-显示，false-隐藏
          "nav_type": "block",
          "nav_img_display_type": "cut", // 显示状态， 普通：normal; 高清： hd; 裁剪: cut; 高清和裁剪： both;
          //规格图宽高比例
          "nav_scale_x": "4", //宽
          "nav_scale_y": "3", //高
          "nav_display_num": "6", //焦点图显示数量
          "nav_position": "center", //焦点小图位置
          "nav_display_padding": 10,
          "nav_step": 1, //切换个数
          "nav_hover": true,

          /*标记*/
          "box_tag_style_0": "e_box",
          "box_tag_style_1": "e_box-000",
          "box_tag_style_2": "p_TagBox",
          "e_img_tag_style_0": "e_image",
          "e_img_tag_style_2": "p_tag",
          "e_img_tag_style_1": "e_image-000",
          "img_tag_display": true, //true 显示标记，false 隐藏标记
          "shiftUpAnimSwitch": false, //是否显示轮播图dom true显示，false不显示
          "img_tag_display_type": "cut", //产品标记 显示状态， 普通：normal; 高清： hd; 裁剪: cut; 高清和裁剪： both;

          //移上动画的配置参数
          //图片遮罩层
          "box_shade_style_0": "e_MatteBox",
          "box_shade_style_1": "e_MatteBox-001",
          "box_shade_style_2": "p_matteA",
          //动画外层
          "box_animatOuter_style_0": "e_MatteBox",
          "box_animatOuter_style_1": "e_MatteBox-000",
          "box_animatOuter_style_2": "p_matte",

          //动画位置层
          "box_Position_style_0": "e_box",
          "box_Position_style_1": "e_PositionBox-001",
          "box_Position_style_2": "p_Position",

          "comp_type": "productList", //组件类型
      },
      sortParams: {
          "DEFAULT_SORT": {
              "v": "默认",
              "title": "默认",
              "style": ""
          },
          "PUBLISH_TIME": {
              "show": true,
              "v": "发布时间",
              "title": "按产品的发布时间进行排序",
              "style": ""
          },
          "PRODUCT_NAME": {
              "show": true,
              "v": " 产品名称",
              "title": "按照产品的名称进行排序",
              "style": ""
          },
          "PVS": {
              "show": false,
              "v": "浏览量",
              "title": "按产品的浏览量进行排序",
              "style": ""
          },
          "comment": {
              "show": false,
              "v": "评论数量",
              "title": "按照产品的评论数量进行排序",
              "style": ""
          },
          "PRODUCT_CODE": {
              "show": true,
              "v": "产品编号",
              "title": "按产品的编号进行排序",
              "style": ""
          }
      },
      styleId: "c_portalResProduct_list-01001011"
  });
</script> 
                            <script>$("div[id^='c_']").attr("loaded","true")</script> 
                          </div>
                            </div>
                      </div>
                        </div>
                  </div>
                    </div>
              </div>
                </div>
          </div>
            </div>
      </div>
        </div>
  </div>
    </div>
</div>
</div>
</div>
</div>

<!--底部--> 

<?php include $this->_include('footer.html'); ?>
