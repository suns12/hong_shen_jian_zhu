<!doctype html>

<html>

<head>

    <meta charset="utf-8">

    <title><?php echo $site_title; ?></title>

    <meta name="keywords" content="<?php echo $site_keywords; ?>" />

    <meta name="description" content="<?php echo $site_description; ?>" />

    <meta name="renderer" content="webkit">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, viewport-fit=cover">

    <link href="<?php echo $site_template; ?>css/swiper.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo $site_template; ?>css/animate.min.css" rel="stylesheet" type="text/css">

    <link href="<?php echo $site_template; ?>css/bootstrap.css" rel="stylesheet" type="text/css">

    <link href="<?php echo $site_template; ?>css/style.css" rel="stylesheet" type="text/css">

    <link href="<?php echo $site_template; ?>css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script type="text/javascript"  src="<?php echo $site_template; ?>js/swiper.min.js"></script>

    <script type="text/javascript"  src="<?php echo $site_template; ?>js/jquery-1.8.3.min.js"></script>

    <script type="text/javascript"  src="<?php echo $site_template; ?>js/wow.min.js"></script>

    <script type="text/javascript"  src="<?php echo $site_template; ?>js/main.js"></script>

    <script type="text/javascript"  src="<?php echo $site_template; ?>js/jquery.SuperSlide.2.1.1.js"></script>

    <script type="text/javascript"  src="<?php echo $site_template; ?>js/wow.min.js"></script>
    

    <script>

        new WOW().init();

    </script>

</head>

<body>

<div class="logo">

    <div class="container">

        <div class="row">

            <div class="logo1 col-8 col-sm-8 col-md-4 col-lg-2 col-xl-2">

                <a href="/"><img src="<?php $this->block(1);?>"></a>

            </div>
			<?php $aaa=$catid;$bbb=$parentid;$ccc=$cat[topid]; ?>
            <div class="col-4 col-sm-4 col-md-8 col-lg-8 col-xl-8">

                <div class="header">

                    <div class="hda">

                      <div class="row">

                                <div class="an"><span class="a1"></span><span class="a2"></span><span class="a3"></span></div>

                                <div class="nav">

                                    <ul>

                                        <li class="<?php if ($index) { ?>on<?php } ?>">

                                            <div class="y_j"><a href="/" class="o_lm">首页</a></div>

                                        </li>

                                        <li class="<?php if($aaa==1 || $bbb==1 || $ccc==1){?>on<?}?>">

                                            <div class="y_j"><?php $return = $this->_category("catid=1");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>

                                            <div class="e_j">

                                                <?php $return = $this->_category("parentid=1");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a>

                                                <?php } ?>

                                            </div>

                                        </li>

                                        <li class="<?php if($aaa==2 || $bbb==2 || $ccc==2){?>on<?}?>">

                                            <div class="y_j"><?php $return = $this->_category("catid=2");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>

                                            <div class="e_j">

                                                <?php $return = $this->_category("parentid=2");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a>

                                                <?php } ?>

                                            </div>

                                        </li>

                                        <li class="<?php if($aaa==3 || $bbb==3 || $ccc==3){?>on<?}?>">

                                            <div class="y_j"><?php $return = $this->_category("catid=3");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>

                                            <div class="e_j">

                                                <?php $return = $this->_category("parentid=3");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a>

                                                <?php } ?>

                                            </div>

                                        </li>

                                        <li class="<?php if($aaa==4 || $bbb==4 || $ccc==4){?>on<?}?>">

                                            <div class="y_j"><?php $return = $this->_category("catid=4");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>

                                            <div class="e_j">

                                                <?php $return = $this->_category("parentid=4");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a>

                                                <?php } ?>

                                            </div>

                                        </li>

                                        <li class="<?php if($aaa==5 || $bbb==5 || $ccc==5){?>on<?}?>">

                                            <div class="y_j"><?php $return = $this->_category("catid=5");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>

                                            <div class="e_j">

                                                <?php $return = $this->_category("parentid=5");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a>

                                                <?php } ?>

                                            </div>

                                        </li>

                                        <li class="<?php if($aaa==6 || $bbb==6 || $ccc==6){?>on<?}?>">

                                            <div class="y_j"><?php $return = $this->_category("catid=6");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>

                                            <div class="e_j">

                                                <?php $return = $this->_category("parentid=6");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a>

                                                <?php } ?>

                                            </div>

                                        </li>

                                        <li class="<?php if($aaa==7 || $bbb==7 || $ccc==7){?>on<?}?>">

                                            <div class="y_j"><?php $return = $this->_category("catid=7");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>" class="o_lm"><?php echo $xiao['catname']; ?></a><?php }  if ($xiao['child']) { ?><i class="fa fa-angle-right"></i><?php } ?></div>

                                            <div class="e_j">

                                                <?php $return = $this->_category("parentid=7");  if (is_array($return))  foreach ($return as $key=>$xiao) { $allchildids = @explode(',', $xiao['allchildids']);    $current = in_array($catid, $allchildids);?><a href="<?php echo $xiao['url']; ?>"><?php echo $xiao['catname']; ?></a>

                                                <?php } ?>

                                            </div>

                                        </li>

                                    </ul>

                                </div>

                                <div class="clear"></div>

                            </div>

                    </div>

                </div>

            </div>

            <div class="logo2 col-sm-12 col-md-2 col-lg-2 col-xl-2">

                <div class="sc"><img src="<?php echo $site_template; ?>images/fd2.png"><a><?php $this->block(2);?></a></div>

            </div>

        </div>

    </div>

</div>

