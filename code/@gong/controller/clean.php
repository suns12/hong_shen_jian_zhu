<?php

class clean extends Admin
{
    private $status_arr;
    private $site_status;
    private $tree;

    public function __construct()
    {
        parent::__construct();
    }
 
    public function indexAction()
    {
        $page  = (int) Request::get('page') ? (int) Request::get('page') : 1;
        $num = 10; // 根据数据量多少修改本参数，如果多就改小点。最小1
        $dir   = XIAOCMS_PATH . 'data/upload/';
        $files = my_scandir($dir);
        $txt   = $this->getlistAction($page);
        if(empty($txt)){
        	$this->show_message('清除成功，分析出来的图片在data\upload\del\目录', 1, '#');
        }
        $model = $this->mAction();
        $i = 0;
        foreach ($txt as $key => $value) {
            if($i==$num){
                continue;
            }
            $r = $this->dirAction($value, $model);
            if ($r) {
                $name = str_replace("/","_",$value);
                rename($dir . $value,$dir.'del/'.$name); // 这个是移动文件
                //unlink($dir . $value); // 这个是直接删除
            }
            unset($txt[$key]);
            $i++;
        }
		set_cache('txt', $txt);
		$page = $page+1;
        $this->show_message('自动执行下一步请不要关闭 '.$page, 1, '?c=clean&page=' .$page);
        return;
    }

    // 删除图片
    public function dirAction($file, $model)
    {
        $file = '/data/upload/' . $file;
        $a    = $this->db->setTableName('content')->getOne('thumb=?', $file);
        if ($a) {
            return false;
        }

        foreach ($model as $table => $field) {
            $w = implode(" LIKE '%{$file}%' or ", $field) . " LIKE '%{$file}%' ";
            $r = $this->db->setTableName($table)->getOne($w);
            if ($r) {
                return false;
            }

        }
        return true;
    }

    // 获取模型字段
    public function mAction()
    {
        $model = $this->db->setTableName('model')->getAll();
        $a     = array();
        foreach ($model as $key => $value) {
            $f = $this->db->setTableName('model_field')->getAll("modelid=? and formtype in('editor','file','files')", $value['modelid'], 'field');
            if ($f) {
                foreach ($f as $key => $ff) {
                    $a[$value['tablename']][] = $ff['field'];
                }
            }
        }
        return $a;
    }
    // 获取模型字段
    public function getlistAction($page)
    {
        if ($page == 1) {

            $dir   = XIAOCMS_PATH . 'data/upload/';
            $files = my_scandir($dir);
            $txt   = array();
            foreach ($files['image'] as $key => $d) {
                if (is_array($d)) {
                    foreach ($d as $keya => $value) {
                        $txt[] = 'image/' . $key . '/' . $value;
                    }
                }
            }
            foreach ($files['file'] as $key => $d) {
                if (is_array($d)) {
                    foreach ($d as $keya => $value) {
                        $txt[] = 'file/' . $key . '/' . $value;
                    }
                }
            }
            set_cache('txt', $txt);

            return $txt;

        } else {
            $txt = get_cache('txt');
            return $txt;
        }
    }
}

function my_scandir($dir)
{
    if (is_dir($dir)) {
        $files      = array();
        $child_dirs = scandir($dir);
        foreach ($child_dirs as $child_dir) {
            //'.'和'..'是Linux系统中的当前目录和上一级目录，必须排除掉，
            //否则会进入死循环，报segmentation falt 错误
            if ($child_dir != '.' && $child_dir != '..') {
                if (is_dir($dir . '/' . $child_dir)) {
                    $files[$child_dir] = my_scandir($dir . '/' . $child_dir);
                } else {
                    $files[] = $child_dir;
                }
            }
        }
        return $files;
    } else {
        return $dir;
    }
}
