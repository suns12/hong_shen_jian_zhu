<?php

class weixinmenu extends Admin {
	private $tree;
    public function __construct() {
		parent::__construct();
		$this->tree =  xiaocms::load_class('Tree');
	}

	public function indexAction() {
		$this->tree->icon = array('&nbsp;&nbsp;&nbsp;│ ','&nbsp;&nbsp;&nbsp;├─ ','&nbsp;&nbsp;&nbsp;└─ ');
		$this->tree->nbsp = '&nbsp;&nbsp;&nbsp;';
		$menu = array();
		$cats =  $this->db->setTableName('weixinmenu')->getAll(null, null, null,'id ASC');
		if(!empty($cats)) {
			foreach($cats as $r) {
				$t = '';
				if($r['parentid']==0) {
					$t = ' | <a href="?c=weixinmenu&a=add&id='.$r['id'].'" >添加子菜单</a>';
				}
				$r['str_manage'] = '<a href="?c=weixinmenu&a=edit&id='.$r['id'].'">编辑</a> | <a href="javascript:confirmurl(\'?c=weixinmenu&a=del&id='.$r['id'].'\',\''.'确定删除 吗？ '.'\')">删除</a>'.$t;
				$menu[$r['id']] = $r;
			}
		}
		$str  = "<tr> 
					<td align='left'>\$id</td>
					<td >\$spacer\$name</td>
					<td>\$url</td>
					<td >\$str_manage</td>
					</tr>";
		$this->tree->init($menu);
		$menu = $this->tree->get_tree(0, $str);	
		include $this->admin_tpl('weixinmenu_list');
	}

	/**
	 * 添加
	 */
	public function addAction() {
	    if (Request::isPost()) {
	        $data = Request::post('data');
			if (!$data['name'] ) $this->show_message('请填写名称',2,1);
			$this->db->setTableName('weixinmenu')->insert($data,true);
			$this->show_message('添加成功', 1, '?c=weixinmenu');
	    }
		$data =  $this->db->setTableName('weixinmenu')->where('parentid=0')->getAll(null, null, null ,'id ASC');
	    $id   = (int)Request::get('id');
		$add  = 1;
		$this->tree->icon = array(' ','  |-','  |-');
		$this->tree->nbsp = '&nbsp;&nbsp;&nbsp;';
		$menu = array();
		if(!empty($data)) {
			foreach($data as $r) { 
				$menu[$r['id']] = $r;
			}
		}
		$str  = "<option value='\$id' \$selected>\$spacer \$name</option>";
		$this->tree->init($menu);
		$menu = $this->tree->get_tree_category(0, $str,'2',$id);
	    include $this->admin_tpl('weixinmenu_add');
	}
	
	/**
	 * 修改栏目
	 */
    public function editAction() {
    	$id =Request::get('id');
        $data = $this->db->setTableName('weixinmenu')->find($id);
        if (empty($data)) $this->show_message('栏目不存在');
        $istype    = $this->db->setTableName('weixinmenu')->getOne('id=?', $id);
	    if ( Request::isPost() ) {
	        $data  = Request::post('data');
	        if (empty($data['name'])) $this->show_message('请填写名称',2,1);
            $this->db->setTableName('weixinmenu')->update($data, 'id=' . $id);
            $this->show_message('修改成功', 1, '?c=weixinmenu&a=index');
	    }
		$cdata =  $this->db->setTableName('weixinmenu')->where('parentid=0')->getAll(null, null, null,'id ASC');
		$this->tree->icon = array(' ','  |-','  |-');
		$this->tree->nbsp = '&nbsp;&nbsp;&nbsp;';
		$menu = array();
		if(!empty($cdata)) {
			foreach($cdata as $r) {
			    $r['disabled'] = ($r['id']==$id) ? 'disabled' : '';
				$menu[$r['id']] = $r;
			}
		}
		$str  = "<option value='\$id' \$selected \$disabled>\$spacer \$name</option>";
		$this->tree->init($menu);
		$menu = $this->tree->get_tree_category(0, $str,'2',$data['parentid']);
	    include $this->admin_tpl('weixinmenu_add');
	}
	
	/**
	 * 删除栏目
	 */
	public function delAction() {
		$ids = Request::get($id);
		$idarr = explode(',', $ids);
	    foreach ($idarr as $id) {
		    if(empty($id)) continue;
	        $this->db->table('weixinmenu')->delete('id=?' , $id);
        }
	    $this->repairAction(); 
        $this->show_message('删除成功', 1, '?c=weixinmenu');

	}
	

	public function postAction() {
		$cats =  $this->db->setTableName('weixinmenu')->getAll(null, null, null,'id ASC');
		$menuList = array();
		foreach ($cats as $key => $value) {
			$parentid = $value['parentid']?$value['parentid']:'';
			$a = array(
				'id'=>$value['id'],
				'pid'=>$parentid,
				'name'=>$value['name'],
				'type'=>'view',
				'code'=>htmlspecialchars_decode($value['url']),
				);
			$menuList[] = $a;
		}
        //树形排布
        $menuList2 = $menuList;
        foreach($menuList as $key=>$menu){
            foreach($menuList2 as $k=>$menu2){
                if($menu['id'] == $menu2['pid']){
                    $menuList[$key]['sub_button'][] = $menu2;
                    unset($menuList[$k]);
                }
            }
        }
        //处理数据
        foreach($menuList as $key=>$menu){
            if(@$menu['type'] == 'view'){
                $menuList[$key]['url'] = $menu['code'];
                $menuList[$key]['url'] = $menuList[$key]['url'];
            }else if(@$menu['type'] == 'click'){
                $menuList[$key]['key'] = $menu['code'];
            }else if(@!empty($menu['type'])){
                $menuList[$key]['key'] = $menu['code'];
                if(!isset($menu['sub_button'])) $menuList[$key]['sub_button'] = array();
            }
            unset($menuList[$key]['code']);
            unset($menuList[$key]['id']);
            unset($menuList[$key]['pid']);
            $menuList[$key]['name'] = urlencode($menu['name']);
            if(isset($menu['sub_button'])){
                unset($menuList[$key]['type']);
                foreach($menu['sub_button'] as $k=>$son){
                    if($son['type'] == 'view'){
                        $menuList[$key]['sub_button'][$k]['url'] = $son['code'];
                        $menuList[$key]['sub_button'][$k]['url'] = $menuList[$key]['sub_button'][$k]['url'];
                    }else if($son['type'] == 'click'){
                        $menuList[$key]['sub_button'][$k]['key'] = $son['code'];
                    }else{
                        $menuList[$key]['sub_button'][$k]['key'] = $son['code'];
                        $menuList[$key]['sub_button'][$k]['sub_button'] = array();
                    }
                    unset($menuList[$key]['sub_button'][$k]['code']);
                    //处理PID和ID
                    unset($menuList[$key]['sub_button'][$k]['id']);
                    unset($menuList[$key]['sub_button'][$k]['pid']);
                    $menuList[$key]['sub_button'][$k]['name'] = urlencode($son['name']);
                }
            }
        }
        //整理格式
        $data = array();
        $menuList = array_values($menuList);
        $data['button'] = $menuList;
		$data = json_encode($data);
		$data = urldecode($data);
		$b = get_token();

		$a = request_post('https://api.weixin.qq.com/cgi-bin/menu/create?access_token='.$b,$data);
        $this->show_message('提交成功', 1, '?c=weixinmenu');
	}
	

}